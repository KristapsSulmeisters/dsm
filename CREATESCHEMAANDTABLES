CREATE SCHEMA `driving_school_management` ;

use driving_school_management;


CREATE TABLE IF NOT EXISTS student_status (
    id INT AUTO_INCREMENT PRIMARY KEY,
    status_name VARCHAR(50)
);

-- students

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(30) NOT NULL,
    last_name VARCHAR(30) NOT NULL,
    phone_number VARCHAR(13) NOT NULL,
    status_id INT NOT NULL,
    personal_code VARCHAR(11) NOT NULL,
    class_id INT NOT NULL,
    CONSTRAINT fk_student_status FOREIGN KEY (status_id)
        REFERENCES student_status (id),
    CONSTRAINT fk_student_class FOREIGN KEY (class_id)
        REFERENCES classes (id)
);

-- locations

CREATE TABLE IF NOT EXISTS locations (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    latitude DECIMAL(10 , 6 ),
    longitud DECIMAL(10 , 6 ),
    image_location VARCHAR(255)
);

    

    -- roles

CREATE TABLE IF NOT EXISTS teacher_roles (
   id INT AUTO_INCREMENT PRIMARY KEY,
    role_name VARCHAR(30) NOT NULL
);

    

    

-- teachers

CREATE TABLE IF NOT EXISTS teachers (
    id INT AUTO_INCREMENT PRIMARY KEY,
    teacher_first_name VARCHAR(50) NOT NULL,
    teacher_last_name VARCHAR(50) NOT NULL,
    role_id INT NOT NULL,
    CONSTRAINT fk_teachers_role FOREIGN KEY (role_id)
        REFERENCES teacher_roles (id)
);

CREATE TABLE IF NOT EXISTS classes (
    id INT AUTO_INCREMENT PRIMARY KEY,
    class_description VARCHAR(50) NOT NULL,
    start_date DATETIME NOT NULL,
    end_date DATETIME NOT NULL,
    teacher_id INT NOT NULL,
    CONSTRAINT fk_class_teacher FOREIGN KEY (teacher_id)
        REFERENCES teachers (id)
);

 

  -- theory attendance

CREATE TABLE IF NOT EXISTS theory_lessons (
    id INT AUTO_INCREMENT PRIMARY KEY,
    date_time_starts DATETIME NOT NULL,
    date_time_ends DATETIME NOT NULL,
    teacher_id INT NOT NULL,
    class_id INT NOT NULL,
    description VARCHAR(50) NOT NULL,
    CONSTRAINT fk_teacher FOREIGN KEY (teacher_id)
        REFERENCES teachers (id),
    CONSTRAINT fk_class FOREIGN KEY (class_id)
        REFERENCES classes (id)
);

CREATE TABLE IF NOT EXISTS theory_attendance (
    theory_attendance_id INT AUTO_INCREMENT PRIMARY KEY,
    student_id INT NOT NULL,
    theory_id INT NOT NULL,
    description VARCHAR(100) NULL,
    CONSTRAINT fk_student_attendance FOREIGN KEY (student_id)
        REFERENCES students ( id),
    CONSTRAINT fk_theory FOREIGN KEY (theory_id)
        REFERENCES theory_lessons (id)
);

CREATE TABLE IF NOT EXISTS practical_lesson_attendance (
    id INT AUTO_INCREMENT PRIMARY KEY,
    student_id INT NOT NULL,
    teacher_id INT NOT NULL,
    lesson_description VARCHAR(50) NOT NULL,
    date_time_start DATETIME NOT NULL,
    academic_hours INT NOT NULL,
    CONSTRAINT fk_student_practical_attendance FOREIGN KEY (student_id)
        REFERENCES students (id),
    CONSTRAINT fk_instructor FOREIGN KEY (teacher_id)
        REFERENCES teachers (id)
);

CREATE TABLE IF NOT EXISTS practical_lesson_mistakes (
    mistake_id INT AUTO_INCREMENT PRIMARY KEY,
    practical_attendance_id INT NOT NULL,
    location_id INT NOT NULL,
    description VARCHAR(50) NOT NULL,
    CONSTRAINT fk_practical_lesson_mistakes FOREIGN KEY (practical_attendance_id)
        REFERENCES practical_lesson_attendance (id),
    CONSTRAINT fk_location FOREIGN KEY (location_id)
        REFERENCES locations (id)
);

show tables;