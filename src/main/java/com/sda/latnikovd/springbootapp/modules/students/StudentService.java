package com.sda.latnikovd.springbootapp.modules.students;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepo studentRepo;

    @Transactional(readOnly = true)
    public List<Student> findAll	() {

        return studentRepo.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Student save(final Student student) {
        if (student.getFirstName() == null) {
            throw new RuntimeException("student's name is undefined");
        }




        return studentRepo.save(student);
    }
}
