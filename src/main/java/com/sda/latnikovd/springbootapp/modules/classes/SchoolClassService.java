package com.sda.latnikovd.springbootapp.modules.classes;




import com.sda.latnikovd.springbootapp.RecordNotFoundException;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SchoolClassService {

    @Autowired
    private SchoolClassRepo schoolClassRepo;

    @Transactional(readOnly = true)
    public List<SchoolClass> findAll() {

        return schoolClassRepo.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public SchoolClass createOrUpdateSchoolClass( SchoolClass schoolClassEntity) throws RecordNotFoundException{
       Optional<SchoolClass> schoolClass = schoolClassRepo.findById(schoolClassEntity.getId());

       LocalDateTime timeNow =  LocalDateTime.now();
        LocalDateTime time48HoursBefore = timeNow.minusHours(48);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(schoolClassEntity.getStartDate(), formatter);
        int compareValues = dateTime.compareTo(time48HoursBefore);


       if(schoolClass.isPresent())
       {
           SchoolClass newSchoolClassEntity = schoolClass.get(); //gets  class
           newSchoolClassEntity.setClassDescription(schoolClassEntity.getClassDescription());//sets new description
           newSchoolClassEntity.setEndDate(schoolClassEntity.getEndDate()); //sets end date
           newSchoolClassEntity.setStartDate(schoolClassEntity.getStartDate());
           newSchoolClassEntity.setTeacherId(schoolClassEntity.getTeacherId());

           //todo: Administrator can not update class start_date if class is already started

           LocalDateTime timeStarted = LocalDateTime.parse( newSchoolClassEntity.getStartDate(), formatter);
           int compareStart = timeStarted.compareTo( timeNow );
           Validate.isTrue( compareStart > 0 ,"Class has already started, can't update" );

           newSchoolClassEntity = schoolClassRepo.save(newSchoolClassEntity);

           return newSchoolClassEntity;


       } else{
           String aaa;
           schoolClassEntity = schoolClassRepo.save(schoolClassEntity);
       }





                Validate.inclusiveBetween(0,2,compareValues, " schoool class can not be created if start date is less than 48 hrs before today");

        // example validate usages
        Validate.notNull(schoolClassEntity, "school class is undefined");
        Validate.notBlank(schoolClassEntity.getClassDescription(), " school class  description is blank for class '%s'", schoolClass);

        return schoolClassRepo.save(schoolClassEntity);
    }

    @Transactional(rollbackFor = Exception.class)
    public SchoolClass getSchoolClassById(Long id ) throws RecordNotFoundException {
        Optional<SchoolClass> schoolClass = schoolClassRepo.findById(id);

        if(schoolClass.isPresent()){
            return schoolClass.get();

        }else{

            throw new RecordNotFoundException("No class exist for particular class ID");
        }

    }

    /*
    @Autowired
    private SchoolClassRepo schoolClassRepo;

    @Transactional(readOnly = true)
    public List<SchoolClass> findAll() {

        return schoolClassRepo.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public SchoolClass save(final SchoolClass schoolClass) {
        LocalDateTime timeNow =  LocalDateTime.now();
        LocalDateTime time48HoursBefore = timeNow.minusHours(48);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(schoolClass.getStartDate(), formatter);
        int compareValues = dateTime.compareTo(time48HoursBefore);

        Validate.inclusiveBetween(0,2,compareValues, " schoool class can not be created if start date is less than 48 hrs before today");

       // example validate usages
        Validate.notNull(schoolClass, "school class is undefined");
        Validate.notBlank(schoolClass.getClassDescription(), " school class  description is blank for class '%s'", schoolClass);
        // you can add validates for all other fields that are notNull in database

//        Administrator can not update class start_date if class
//        is already started



        return schoolClassRepo.save(schoolClass);
    }


    @Transactional(rollbackFor = Exception.class)
    public  void deleteById(final Long id){

        List<SchoolClass> listOfSchoolRepos =  schoolClassRepo.findAll();


        listOfSchoolRepos.stream().filter( el->el.getId().equals( id ) )

                .collect( Collectors.toList());

       for(int i = 0 ; i < listOfSchoolRepos.size(); i++){

            if(listOfSchoolRepos.get( i ).getId().equals( id )){
                break;
            }
            else {
              Validate.notNull( listOfSchoolRepos.get( i ).getId(),"record with that id does not exists"  )  ;
            }
        }

        Validate.notNull( listOfSchoolRepos,"record with that id does not exists" );
        Long i  = id;

        String aaa = "aaa";

        schoolClassRepo.deleteById(id);
      }

    @Transactional(rollbackFor = Exception.class)
    public  void findById(Long id) {
        schoolClassRepo.findById( id );
    }
*/

}
