package com.sda.latnikovd.springbootapp.modules.teacherrole;

import com.sda.latnikovd.springbootapp.modules.classes.SchoolClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// please note that repository class is package private to minimize it's visibility
// only services should use corresponding repository classes!
@Repository
interface TeacherRoleRepo extends JpaRepository<TeacherRole, Long> {

}
