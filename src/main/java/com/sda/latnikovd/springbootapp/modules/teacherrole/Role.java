package com.sda.latnikovd.springbootapp.modules.teacherrole;

public enum Role {

    THEORY,
    DRIVING;

    // missing toString method


     @Override
     public String toString() {
         return "Role{}";
     }
}
