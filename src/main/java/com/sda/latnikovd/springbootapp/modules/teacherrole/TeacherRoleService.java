package com.sda.latnikovd.springbootapp.modules.teacherrole;





import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TeacherRoleService {
    @Autowired
    private TeacherRoleRepo teacherRoleRepo;

    @Transactional(readOnly = true)
    public List<TeacherRole> findAll() {

        return teacherRoleRepo.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public TeacherRole save(final TeacherRole teacherRole) {
        if (teacherRole.getRole() == null) {
            throw new RuntimeException(" role is undefined");
        }

        // example validate usages
        Validate.notNull(teacherRole, "teacher role is undefined");
        Validate.notBlank(teacherRole.getRole().toString(), " school class  description is blank for class '%s'", teacherRole);

        return teacherRoleRepo.save(teacherRole);
    }

}
