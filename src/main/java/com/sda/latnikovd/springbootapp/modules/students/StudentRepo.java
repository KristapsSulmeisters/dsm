package com.sda.latnikovd.springbootapp.modules.students;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {
}




