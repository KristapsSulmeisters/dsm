package com.sda.latnikovd.springbootapp.modules.teacherrole;


import javax.persistence.*;

@Entity
@Table(name = "teacher_roles")
public class TeacherRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "role_name")
    @Enumerated(EnumType.STRING)
    private Role role;


    public TeacherRole(Long id, Role role) {
        this.id = id;
        this.role = role;
    }

    public TeacherRole() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

}
