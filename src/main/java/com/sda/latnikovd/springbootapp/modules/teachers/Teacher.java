package com.sda.latnikovd.springbootapp.modules.teachers;


import com.sda.latnikovd.springbootapp.modules.teacherrole.TeacherRole;

import javax.persistence.*;

@Entity
@Table(name = "teachers")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "role_id")
    private Long roleId;



    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id", referencedColumnName = "id", insertable = false,updatable = false)
    private TeacherRole teacherRole;

    public Teacher() {
    }

    public Teacher(Long id, String firstName, String lastName, Long roleId, TeacherRole teacherRole) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roleId = roleId;
        this.teacherRole = teacherRole;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public TeacherRole getTeacher() {
        return teacherRole;
    }

    public void setTeacherRole(TeacherRole teacherRole) {
        this.teacherRole = teacherRole;
    }

/*
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "role_id")
    private Long roleId;



    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id", referencedColumnName = "id", insertable = false, updatable = false)
    @Enumerated(EnumType.ORDINAL)
    private TeacherRole teacher;

    public Teacher() {
    }

    public Teacher(Long id, String firstName, String lastName, Long roleId, TeacherRole teacher) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.roleId = roleId;
        this.teacher = teacher;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public TeacherRole getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherRole teacher) {
        this.teacher = teacher;
    }

    // missing a toString method in entity is generally a bad practice- since it will be harder debugging
    */
}
