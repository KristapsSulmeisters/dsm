package com.sda.latnikovd.springbootapp.modules.classes;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// please note that repository class is package private to minimize it's visibility
// only services should use corresponding repository classes!
@Repository
interface SchoolClassRepo extends JpaRepository<SchoolClass, Long> {

     //void deleteSchoolClassesById(Long id);
   //  List<SchoolClass> findByClassDescription(String description);


}
