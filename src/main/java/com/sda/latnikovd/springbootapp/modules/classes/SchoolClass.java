package com.sda.latnikovd.springbootapp.modules.classes;

import com.sda.latnikovd.springbootapp.modules.teachers.Teacher;

import javax.persistence.*;

@Entity
@Table(name = "classes")
public class SchoolClass {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "class_description")
	private String classDescription;

	@Column(name = "start_date")
	private String startDate;

	@Column(name = "end_date")
	private String endDate;

	@Column(name = "teacher_id")
	private Long teacherId;


	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "teacher_id", referencedColumnName = "id", insertable = false,updatable = false)
	private Teacher teacher;

	public SchoolClass() {
	}


	public SchoolClass(Long id, String classDescription, String startDate, String endDate, Long teacherId) {
		this.id = id;
		this.classDescription = classDescription;
		this.startDate = startDate;
		this.endDate = endDate;
		this.teacherId = teacherId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getClassDescription() {
		return classDescription;
	}

	public void setClassDescription(String classDescription) {
		this.classDescription = classDescription;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	@Override
	public String toString() {
		return "SchoolClass{" +
				"id=" + id +
				", classDescription='" + classDescription + '\'' +
				", startDate='" + startDate + '\'' +
				", endDate='" + endDate + '\'' +
				", teacherId=" + teacherId +
				'}';
	}

}
