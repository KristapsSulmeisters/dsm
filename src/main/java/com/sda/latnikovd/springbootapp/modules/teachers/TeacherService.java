package com.sda.latnikovd.springbootapp.modules.teachers;



 import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
 import java.util.Optional;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepo teacherRepo;

    @Transactional(readOnly = true)
    public List<Teacher> findAll() {
        return teacherRepo.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Teacher save(final Teacher teacher) {
        if (teacher.getFirstName() == null) {
            throw new RuntimeException(" teachers name is undefined");
        }

        // example validate usages
        Validate.notNull(teacher, "teacher is undefined");
        Validate.notBlank(teacher.getFirstName(), " teachers name is blank for teacher '%s'", teacher);

        return teacherRepo.save(teacher);
    }

   /* @Autowired
    private TeacherRepo teacherRepo;



    @Transactional(readOnly = true)
    public List<Teacher> findAll() {
        return teacherRepo.findAll();
    }

    @Transactional(rollbackFor = Exception.class)
    public Teacher save(final Teacher teacher) {
//        if (teacher.getFirstName() == null) {
//            throw new RuntimeException(" teachers name is undefined");
//        }
        //Administrator can not update class start_date if class
        //is already started
        // example validate usages
        Validate.notNull(teacher, "teacher is undefined");
        Validate.notBlank(teacher.getFirstName(), " teachers name is blank for teacher '%s'", teacher);

        return teacherRepo.save(teacher);
    }
    @Transactional(rollbackFor = Exception.class)
    public Optional<Teacher> findById(long id) {

        return teacherRepo.findById( id );
    }*/
}
