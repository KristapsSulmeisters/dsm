package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.teachers.Teacher;
import com.sda.latnikovd.springbootapp.modules.teachers.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/teachers")
public class TeacherController {

	@Autowired
	private TeacherService teacherService;

	@GetMapping
	public List<Teacher> findAll() {
		return teacherService.findAll();
	}

	@PutMapping
	public Teacher create(@RequestBody Teacher teacher) {
		return teacherService.save(teacher);
	}

	@PatchMapping
	public Teacher update(@RequestBody Teacher teacher) {
		return teacherService.save(teacher);
	}

}
