package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.RecordNotFoundException;
import com.sda.latnikovd.springbootapp.modules.classes.SchoolClass;
import com.sda.latnikovd.springbootapp.modules.classes.SchoolClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/classes")
public class SchoolClassController {

	@Autowired
	private SchoolClassService schoolClassService;

	@GetMapping //
	public List<SchoolClass> findAll() {
		return schoolClassService.findAll();
	}

	@GetMapping ("/{id}") //by id
	public  SchoolClass findById(@PathVariable Long id) throws RecordNotFoundException {

		return schoolClassService.getSchoolClassById(id);
	}

	@PutMapping
	public SchoolClass create(@RequestBody SchoolClass schoolClass) throws RecordNotFoundException {
		return schoolClassService.createOrUpdateSchoolClass(schoolClass);
	}

	@PatchMapping
	public SchoolClass update(@RequestBody SchoolClass schoolClass) throws RecordNotFoundException {
		return schoolClassService.createOrUpdateSchoolClass(schoolClass);
	}

	/*@DeleteMapping("/{id}")
    public SchoolClass delete()
*/


}
