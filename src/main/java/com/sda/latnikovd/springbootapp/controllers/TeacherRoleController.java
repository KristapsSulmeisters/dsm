package com.sda.latnikovd.springbootapp.controllers;

import com.sda.latnikovd.springbootapp.modules.teacherrole.TeacherRole;
import com.sda.latnikovd.springbootapp.modules.teacherrole.TeacherRoleService;
import com.sda.latnikovd.springbootapp.modules.teachers.Teacher;
import com.sda.latnikovd.springbootapp.modules.teachers.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher_roles")
public class TeacherRoleController {

	@Autowired
	private TeacherRoleService teacherRoleService;

	@GetMapping
	public List<TeacherRole> findAll() {
		return teacherRoleService.findAll();
	}

	@PutMapping
	public TeacherRole create(@RequestBody TeacherRole teacherRole) {
		return teacherRoleService.save(teacherRole);
	}

	@PatchMapping
	public TeacherRole update(@RequestBody TeacherRole teacherRole) {
		return teacherRoleService.save(teacherRole);
	}
}
